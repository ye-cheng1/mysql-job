-- 1.查询"01"课程比"02"课程成绩高的 学生的信息 及课程分数

SELECT * from sc A ,sc B, student C
WHERE A.sid=B.sid and A.sid =C.sid and A.cid=01 and B.cid=02 and A.score>B.score



-- 1.1 查询存在" 01 "课程但可能不存在" 02 "课程的情况(不存在时显示为 null )

SELECT * FROM sc A
LEFT join sc B on A.sid = B.sid and B.cid=02
WHERE A.cid=01 



-- 1.2 查询同时存在01和02课程的情况

SELECT * from sc A ,sc B
WHERE A.sid=B.sid and A.cid=01 and B.cid=02



-- 1.3 查询选择了02课程但没有01课程的情况
SELECT
	* 
FROM
	sc 
WHERE
	sid  not IN ( SELECT sid FROM sc WHERE cid = 01 ) and cid =02
	
SELECT sid from sc WHERE cid =01



-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩

select A.sid,COUNT(A.sid),SUM(A.score),(SUM(A.score)/COUNT(A.sid)),B.* from sc A
INNER join student B on A.sid=B.sid
GROUP BY A.sid having (SUM(score)/COUNT(A.sid))>60


-- 3.查询在 SC 表存在成绩的学生信息
select * from student where sid in
(
	SELECT DISTINCT sid  from sc
)
;

-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的成绩总和


select student.sid,student.sname,IFNULL(temp.选课总数,0)选课总数,IFNULL(temp.成绩总和,0)成绩总和 from student
LEFT join 
(
	SELECT sid,count(*)选课总数,sum(score)成绩总和 from sc 
	GROUP BY sid
)temp on temp.sid=student.sid

-- 5.查询「李」姓老师的数量

SELECT count(*) 数量 from teacher where tname like '李%';

-- 6.查询学过「张三」老师授课的同学的信息
SELECT * from student where sid in
(
SELECT sid from sc where cid =
(
SELECT cid from course where tid =
(
SELECT tid from teacher where tname = '张三'
)
)
);

-- 7.查询没有学全所有课程的同学的信息
select * from student where sid in
(
SELECT sid from sc GROUP BY sid having COUNT(*) !=
(
SELECT COUNT(*) from course 
)
);


-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息

select * from student where sid in
(
SELECT sid from sc where cid in
(
SELECT cid from sc where sid = '01'
)
) GROUP BY sname ORDER BY sid;






-- 9.查询和" 01 "号的同学学习的课程完全相同的其他同学的信息

SELECT * from student where sid not in
(
select sc.sid from sc LEFT join 
(
SELECT * from sc where sid = '01'
)temp on sc.cid = temp.cid 
where temp.sid is null
ORDER BY sc.sid,sc.cid
)
and sid in
(
SELECT sid from sc GROUP BY sid HAVING COUNT(*) =
(
SELECT COUNT(*) from sc where sid = '01'
)
);

-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名
SELECT sname from student where sid not in
(
SELECT sid from sc where cid in
(
SELECT cid from course where tid in
(
SELECT tid from teacher where tname = '张三'
)
)
);

-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩
select * from (
SELECT sid,sname from student where sid in
(
SELECT sid from sc where score<60 GROUP BY sid HAVING  COUNT(*)>=2
))A inner join 
(
SELECT sid,avg(score) from sc where sid in
(
SELECT sid from sc where score<60 GROUP BY sid
) GROUP BY sid)B on A.sid = B.sid;



-- 12.检索" 01 "课程分数小于 60，按分数降序排列的学生信息
SELECT * from student where sid in
(
select sid from sc where cid=01 and score<60 ORDER BY score DESC
)


-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩
#学生的平均成绩
SELECT b.*,a.平均成绩 from 
(
	SELECT sid,ROUND(AVG(score),2)平均成绩 from sc
	GROUP BY sid
)a
right join 
(
	SELECT s.*,sc1.score 语文成绩,sc2.score 数学成绩,sc3.score 英语成绩 from student s
	LEFT join sc sc1 on sc1.sid=s.sid and sc1.cid='01'
	LEFT join sc sc2 on sc2.sid=s.sid and sc2.cid='02'
	LEFT join sc sc3 on sc3.sid=s.sid and sc3.cid='03'
)b
on a.sid=b.sid
ORDER BY a.平均成绩 desc



-- 14.查询各科成绩最高分、最低分和平均分,以如下形式显示：
-- 课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
-- 及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90
-- 要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序
-- 排列
SELECT A.cid,cname,最高分,最低分,平均分,IFNULL(及格率,0) 及格率,IFNULL(中等率,0) 中等率,IFNULL(优良率,0) 优良率,IFNULL(优秀率,0) 优秀率 from
(
select a.cid,及格人数/总人数 及格率 from 
(
SELECT cid,COUNT(cid) 及格人数 from sc where score >=60 GROUP BY cid 
)a inner join
(
SELECT cid,COUNT(cid) 总人数 from sc GROUP BY cid 
)b on a.cid = b.cid
)A LEFT JOIN 
(
select a.cid,中等人数/总人数 中等率 from 
(
SELECT cid,COUNT(cid) 中等人数 from sc where 80>= score and score>=70 GROUP BY cid 
)a inner join
(
SELECT cid,COUNT(cid) 总人数 from sc GROUP BY cid 
)b on a.cid = b.cid
)B  ON A.cid = B.cid LEFT JOIN
(
select a.cid,优良人数/总人数 优良率 from 
(
SELECT cid,COUNT(cid) 优良人数 from sc where score >=80 and score <=90 GROUP BY cid 
)a inner join
(
SELECT cid,COUNT(cid) 总人数 from sc GROUP BY cid 
)b on a.cid = b.cid
)C  ON A.cid = C.cid LEFT JOIN
(
select a.cid,优秀人数/总人数 优秀率 from 
(
SELECT cid,COUNT(cid) 优秀人数 from sc where score >=90 and score GROUP BY cid 
)a inner join
(
SELECT cid,COUNT(cid) 总人数 from sc GROUP BY cid 
)b on a.cid = b.cid
)D ON A.cid = D.cid inner join 
(
select cid,max(score) 最高分,min(score) 最低分,AVG(score) 平均分 from sc GROUP BY cid
)temp on temp.cid = A.cid INNER join
course on A.cid = course.cid
ORDER BY A.cid asc;


-- 15.按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺

SELECT sc.*,IFNULL(t.名次,1) 名次 FROM sc 
left join 
(
	SELECT sc1.sid,sc1.cid ,sc1.score ,count(sc1.score)+1 名次 from  sc sc1
	inner join sc sc2 on sc1.cid =sc2.cid and sc1.sid!=sc2.sid and sc1.score<sc2.score
	GROUP BY sc1.sid,sc1.cid,sc1.score 
	order by sc1.score desc
)
t
on t.sid=sc.sid and t.cid=sc.cid
order by sc.cid asc, 名次 asc

-- 15.1 按各科成绩进行行排序，并显示排名， Score 重复时合并名次
SELECT sc.*,IFNULL(t.名次,1) 名次 FROM sc 
left join 
(
	SELECT sc1.sid,sc1.cid ,sc1.score ,count(sc1.score)+1 名次 from  sc sc1
	inner join sc sc2 on sc1.cid =sc2.cid and sc1.sid!=sc2.sid and sc1.score<sc2.score
	GROUP BY sc1.sid,sc1.cid,sc1.score 
	order by sc1.score desc
)
t
on t.sid=sc.sid and t.cid=sc.cid
order by sc.cid asc, 名次 asc;

-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺
SELECT temp1.sid,总成绩,IFNULL(名次,1) 名次 FROM
(
SELECT sid,sum(score) 总成绩 from sc GROUP BY sid
)temp1 LEFT JOIN 
(
select A.sid,count(A.总成绩)+1 名次 from(
SELECT sid,sum(score) 总成绩 from sc GROUP BY sid
)A inner join 
(
SELECT sid,sum(score) 总成绩 from sc GROUP BY sid
)B on A.sid != B.sid and A.总成绩 < B.总成绩
GROUP BY A.sid
)temp2 on temp1.sid = temp2.sid
ORDER BY 名次;


-- 17. 统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比

SELECT A.cid 课程编号,cname 课程名称,
IFNULL(零到六十人数,0) 零到六十人数,IFNULL(零到六十人所占百分比,0) 零到六十人所占百分比,
IFNULL(六十人到七十数,0) 六十人到七十数,IFNULL(六十人到七十所占百分比,0) 六十人到七十所占百分比,
IFNULL(七十到八十五人数,0) 七十到八十五人数,IFNULL(七十到八十五所占百分比,0) 七十到八十五所占百分比,
IFNULL(八十到一百人数,0) 八十到一百人数,IFNULL(八十到一百所占百分比,0) 八十到一百所占百分比 from
(
select a.cid,零到六十人数,零到六十人数/总人数 零到六十人所占百分比 from 
(
SELECT cid,COUNT(cid) 零到六十人数 from sc where score >0 and score<=60 GROUP BY cid 
)a inner join
(
SELECT cid,COUNT(cid) 总人数 from sc GROUP BY cid 
)b on a.cid = b.cid
)A LEFT JOIN 
(
select a.cid,六十人到七十数,六十人到七十数/总人数 六十人到七十所占百分比 from 
(
SELECT cid,COUNT(cid) 六十人到七十数 from sc where score >60 and score<=70 GROUP BY cid 
)a inner join
(
SELECT cid,COUNT(cid) 总人数 from sc GROUP BY cid 
)b on a.cid = b.cid
)B  ON A.cid = B.cid LEFT JOIN
(
select a.cid,七十到八十五人数,七十到八十五人数/总人数 七十到八十五所占百分比 from 
(
SELECT cid,COUNT(cid) 七十到八十五人数 from sc where score >70 and score<=85 GROUP BY cid 
)a inner join
(
SELECT cid,COUNT(cid) 总人数 from sc GROUP BY cid 
)b on a.cid = b.cid
)C  ON A.cid = C.cid LEFT JOIN
(
select a.cid,八十到一百人数,八十到一百人数/总人数 八十到一百所占百分比 from 
(
SELECT cid,COUNT(cid) 八十到一百人数 from sc where score >60 and score<=70 GROUP BY cid 
)a inner join
(
SELECT cid,COUNT(cid) 总人数 from sc GROUP BY cid 
)b on a.cid = b.cid
)D ON A.cid = D.cid inner join 
course on A.cid = course.cid;



-- 18.查询各科成绩前三名的记录
(
SELECT * from sc where cid= '01' ORDER BY score desc LIMIT 3
)union all
(
SELECT * from sc where cid= '02' ORDER BY score desc LIMIT 3
)union all
(
SELECT * from sc where cid= '03' ORDER BY score desc LIMIT 3
);

****************************************************************
SELECT * from(
SELECT sc.*,IFNULL(t.名次,1) 名次 FROM sc 
left join 
(
	SELECT sc1.sid,sc1.cid ,sc1.score ,count(sc1.score)+1 名次 from  sc sc1
	inner join sc sc2 on sc1.cid =sc2.cid and sc1.sid!=sc2.sid and sc1.score<sc2.score
	GROUP BY sc1.sid,sc1.cid,sc1.score 
	order by sc1.score desc
)
t
on t.sid=sc.sid and t.cid=sc.cid
)A
WHERE
	   ( cid = '01' AND 名次 <= 3 )
	OR ( cid = '02' AND 名次 <= 3 ) 
	OR ( cid = '03' AND 名次 <= 3 )
ORDER BY cid

****************************************************************

-- 19.查询每门课程被选修的学生数
SELECT cid,count(sid) from sc GROUP BY cid;


-- 20.查询出只选修两门课程的学生学号和姓名
SELECT sid from sc GROUP BY sid HAVING count(cid) = '02';



-- 21. 查询男生、女生人数
(
SELECT ssex,count(*) from student where ssex = '男'
)UNION ALL
(
SELECT ssex,count(*) from student where ssex = '女'
);


-- 22. 查询名字中含有「风」字的学生信息
SELECT * from student where sname like '%风%';


-- 23查询同名同性学生名单，并统计同名人数
SELECT sname,COUNT(*) 人数 from student GROUP BY sname HAVING COUNT(*)>=2;


-- 24.查询 1990 年出生的学生名单
SELECT sname from student where sage like '1990%';


-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列
SELECT cid,avg(score) from sc GROUP BY cid ORDER BY AVG(score) desc,cid asc

-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩
SELECT student.sid,sname,平均成绩 from student inner join
(
SELECT sid,avg(score) 平均成绩 from sc GROUP BY sid HAVING avg(score)>=85
)temp on temp.sid = student.sid;


-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数
SELECT sname,score from student inner join
(
SELECT sid,score from sc where cid =
(
SELECT cid from course where cname = '数学'
) and score <60
)temp on temp.sid = student.sid;


-- 28. 查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）




-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数
SELECT sname,cname,score from
(
SELECT sid,cid,score from sc where score >70
)A inner join
student on A.sid = student.sid inner join
course on course.cid = A.cid;


-- 30.查询不及格的课程
SELECT sname,cname,score from
(
SELECT sid,cid,score from sc where score <60
)A inner join
student on A.sid = student.sid inner join
course on course.cid = A.cid;


-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名
SELECT sid,sname from student where sid in
(
SELECT sid from sc where cid = '01' and score > 80
);


-- 32.求每门课程的学生人数
SELECT cid,COUNT(*) from sc GROUP BY cid;



-- 33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩
SELECT student.*,score from student inner join 
(
SELECT sid,score from sc where cid =
(
SELECT cid from course where tid =(
SELECT tid from teacher where tname ='张三'
)
) ORDER BY score desc LIMIT 1
)temp on temp.sid = student.sid;

-- 34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生
SELECT student.*,score from student inner join 
(
SELECT sid,score from sc where score =
(
SELECT score from sc where cid =
(
SELECT cid from course where tid =
(
SELECT tid from teacher where tname ='张三'
)
) ORDER BY score desc LIMIT 1
)
)temp on temp.sid = student.sid;


-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩
SELECT sc1.sid,sc1.cid,sc1.score from sc sc1 inner join
sc sc2 on sc1.sid != sc2.sid and sc1.cid != sc2.cid 
where sc1.score = sc2.score;



-- 36. 查询每门成绩最好的前两名
(
SELECT * from sc where cid= '01' ORDER BY score desc LIMIT 2
)union all
(
SELECT * from sc where cid= '02' ORDER BY score desc LIMIT 2
)union all
(
SELECT * from sc where cid= '03' ORDER BY score desc LIMIT 2
);


****************************************************************
SELECT * from(
SELECT sc.*,IFNULL(t.名次,1) 名次 FROM sc 
left join 
(
	SELECT sc1.sid,sc1.cid ,sc1.score ,count(sc1.score)+1 名次 from  sc sc1
	inner join sc sc2 on sc1.cid =sc2.cid and sc1.sid!=sc2.sid and sc1.score<sc2.score
	GROUP BY sc1.sid,sc1.cid,sc1.score 
	order by sc1.score desc
)
t
on t.sid=sc.sid and t.cid=sc.cid
)A
WHERE
	   ( cid = '01' AND 名次 <= 2 )
	OR ( cid = '02' AND 名次 <= 2 ) 
	OR ( cid = '03' AND 名次 <= 2 )
ORDER BY cid

****************************************************************

-- 37. 统计每门课程的学生选修人数（超过 5 人的课程才统计）
SELECT cid,COUNT(*) from sc GROUP BY cid HAVING COUNT(*)>5;



-- 38.检索至少选修两门课程的学生学号
SELECT sid from sc GROUP BY sid HAVING COUNT(*)>=2

-- 39.查询选修了全部课程的学生信息
SELECT * from student where sid in
(
SELECT sid from sc GROUP BY sid HAVING COUNT(*) =
(
SELECT COUNT(*) from course 
)
);



-- 40.查询各学生的年龄，只按年份来算

select
	*,year(now()) - year(sage) `年龄`
from
	student;

-- 41. 按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一
select
	a.sid,a.sname,a.sage,a.ssex,a.`年龄`
from
	(
		select
			*,
			@age := year(now()) - year(sage),
			case
				when month(now()) < month(sage) then @age := @age - 1
				when month(now()) = month(sage) then if(day(now()) < day(sage), @age := @age - 1,@age := @age)
				else @age
			END `年龄`
		from
			student
	) a;


-- 42.查询本周过生日的学生
select
	*, week(sage)
from
	student
where
	week(sage) = week(now());



-- 43. 查询下周过生日的学生
select
	*, week(sage)
from
	student
where
	week(sage) = week(now())+1;

-- 44.查询本月过生日的学生
select *, month(sage) from student;
select month(now());

select
	*, month(sage)
from
	student
where
	month(sage) = month(now());

-- 45.查询下月过生日的学生
select *, month(sage) from student;
select month(now());

select
	*, month(sage)
from
	student
where
	month(sage) = month(now())+1;