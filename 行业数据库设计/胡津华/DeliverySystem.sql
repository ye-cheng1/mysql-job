/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/7 17:07:14                            */
/*==============================================================*/


drop table if exists "Order";

drop table if exists goods_table;

drop table if exists merchant_table;

drop table if exists rider_table;

drop table if exists user_table;

/*==============================================================*/
/* Table: "Order"                                               */
/*==============================================================*/
create table "Order"
(
   order_id             int not null auto_increment,
   user_id              int not null,
   rider_id             int not null,
   evaluate             varchar(30)  not null,
   order_status         varchar(30)  not null,
   ordertime            datetime not null,
   primary key (order_id)
);

/*==============================================================*/
/* Table: goods_table                                           */
/*==============================================================*/
create table goods_table
(
   goods_id             int not null auto_increment,
   merchant_id          int not null,
   goods_name           varchar(30) not null,
   sale_num             int not null,
   primary key (goods_id)
);

/*==============================================================*/
/* Table: merchant_table                                        */
/*==============================================================*/
create table merchant_table
(
   merchant_id          int not null auto_increment,
   order_id             int,
   merchant_name        varchar(30) not null,
   merchant_address     varchar(50) not null,
   merchant_type        varchar(30) not null,
   primary key (merchant_id)
);

/*==============================================================*/
/* Table: rider_table                                           */
/*==============================================================*/
create table rider_table
(
   rider_id             int not null auto_increment,
   order_id             int,
   rider_name           varchar(30) not null,
   ride_credit          enum(2) not null,
   rider_health         enum(2) not null,
   rider_phone          varchar(11) not null,
   primary key (rider_id)
);

/*==============================================================*/
/* Table: user_table                                            */
/*==============================================================*/
create table user_table
(
   user_id              int not null auto_increment,
   order_id             int,
   user_address         varchar(50) not null,
   user_name            varchar(30) not null,
   user_phone           varchar(12) not null,
   primary key (user_id)
);

alter table goods_table add constraint FK_Reference_4 foreign key (merchant_id)
      references merchant_table (merchant_id) on delete restrict on update restrict;

alter table merchant_table add constraint FK_Reference_5 foreign key (order_id)
      references "Order" (order_id) on delete restrict on update restrict;

alter table rider_table add constraint FK_Reference_3 foreign key (order_id)
      references "Order" (order_id) on delete restrict on update restrict;

alter table user_table add constraint FK_Reference_2 foreign key (order_id)
      references "Order" (order_id) on delete restrict on update restrict;

