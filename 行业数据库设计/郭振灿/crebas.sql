/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/7 17:32:32                            */
/*==============================================================*/


drop index Index_1 on 商品表;

drop table if exists 商品表;

drop index Index_1 on 商家表;

drop table if exists 商家表;

drop index Index_1 on 用户表;

drop table if exists 用户表;

drop index Index_3 on 订单表;

drop index Index_2 on 订单表;

drop index Index_1 on 订单表;

drop table if exists 订单表;

drop index Index_1 on 骑手表;

drop table if exists 骑手表;

/*==============================================================*/
/* Table: 商品表                                                   */
/*==============================================================*/
create table 商品表
(
   goods_id             int not null auto_increment,
   merchant_id          int,
   goods_name           varchar(20) not null,
   "goods_ sales"       int not null,
   primary key (goods_id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on 商品表
(
   goods_name
);

/*==============================================================*/
/* Table: 商家表                                                   */
/*==============================================================*/
create table 商家表
(
   merchant_id          int not null auto_increment,
   merchant_name        varchar(20) not null,
   merchant_address     varchar(20) not null,
   merchant_type        varchar(20) not null,
   goods_id             int not null,
   primary key (merchant_id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on 商家表
(
   merchant_name
);

/*==============================================================*/
/* Table: 用户表                                                   */
/*==============================================================*/
create table 用户表
(
   user_id              int not null auto_increment,
   user_address         varchar(20) not null,
   user_name            varchar(20) not null,
   user_tel             char(11) not null,
   primary key (user_id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on 用户表
(
   user_name
);

/*==============================================================*/
/* Table: 订单表                                                   */
/*==============================================================*/
create table 订单表
(
   order_id             int not null auto_increment,
   merchant_id          int not null,
   user_id              int not null,
   rider_id             int not null,
   user_evaluation      varchar(20) not null default NULL,
   order_status         varchar(20) not null default NULL,
   order_time           varchar(20) not null,
   primary key (order_id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on 订单表
(
   merchant_id
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create unique index Index_2 on 订单表
(
   user_id
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create unique index Index_3 on 订单表
(
   rider_id
);

/*==============================================================*/
/* Table: 骑手表                                                   */
/*==============================================================*/
create table 骑手表
(
   rider_id             int not null auto_increment,
   rider_name           varchar(20) not null,
   credit_standing      enum(20) not null,
   health_status        enum(20) not null,
   rider_tel            char(11) not null,
   primary key (rider_id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on 骑手表
(
   rider_name
);

alter table 商品表 add constraint FK_Reference_2 foreign key (merchant_id)
      references 商家表 (merchant_id) on delete restrict on update restrict;

alter table 订单表 add constraint FK_Reference_1 foreign key (user_id)
      references 用户表 (user_id) on delete restrict on update restrict;

alter table 订单表 add constraint FK_Reference_3 foreign key (rider_id)
      references 骑手表 (rider_id) on delete restrict on update restrict;

