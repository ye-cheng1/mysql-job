drop database IF EXISTS yy;
create database yy;
use yy;
/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : localhost:3306
 Source Schema         : hospital

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 07/09/2021 19:56:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for check_order
-- ----------------------------
DROP TABLE IF EXISTS `check_order`;
CREATE TABLE `check_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `result` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结果',
  `master_id` int(11) NULL DEFAULT NULL COMMENT '创建人员ID',
  `cheker_id` int(11) NULL DEFAULT NULL COMMENT '检测人员ID',
  `sick_person_id` int(11) NULL DEFAULT NULL COMMENT '患者ID',
  `machine_id` int(11) NULL DEFAULT NULL COMMENT '仪器ID',
  `see_doctor_order_id` int(11) NULL DEFAULT NULL COMMENT '门诊订单ID',
  `count` decimal(24, 6) NULL DEFAULT NULL COMMENT '次数',
  `price` decimal(24, 6) NULL DEFAULT NULL COMMENT '金额',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `sent_time` datetime(0) NULL DEFAULT NULL COMMENT '完成时间',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `check_machine`(`machine_id`) USING BTREE,
  INDEX `check_sick`(`sick_person_id`) USING BTREE,
  INDEX `check_see`(`see_doctor_order_id`) USING BTREE,
  INDEX `check_woker`(`cheker_id`) USING BTREE,
  INDEX `check_creater`(`master_id`) USING BTREE,
  CONSTRAINT `check_machine` FOREIGN KEY (`machine_id`) REFERENCES `machine` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `check_see` FOREIGN KEY (`see_doctor_order_id`) REFERENCES `see_doctor_order` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `check_sick` FOREIGN KEY (`sick_person_id`) REFERENCES `see_doctor_order` (`sick_person_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `check_woker` FOREIGN KEY (`cheker_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `check_creater` FOREIGN KEY (`master_id`) REFERENCES `see_doctor_order` (`doctor_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '检测订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of check_order
-- ----------------------------


-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `master_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主管ID',
  `parent_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上级ID',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department
-- ----------------------------


-- ----------------------------
-- Table structure for department_room
-- ----------------------------
DROP TABLE IF EXISTS `department_room`;
CREATE TABLE `department_room`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `department_id` int(11) NULL DEFAULT NULL COMMENT '部门ID',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '上级科室ID',
  `master_id` int(11) NULL DEFAULT NULL COMMENT '主管ID',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `dept_room_woker`(`master_id`) USING BTREE,
  INDEX `dept_room_dept`(`department_id`) USING BTREE,
  CONSTRAINT `dept_room_woker` FOREIGN KEY (`master_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dept_room_dept` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '科室信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department_room
-- ----------------------------


-- ----------------------------
-- Table structure for drug
-- ----------------------------
DROP TABLE IF EXISTS `drug`;
CREATE TABLE `drug`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品名',
  `brand` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌',
  `count` decimal(24, 6) NULL DEFAULT NULL COMMENT '数量',
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `recive_time` datetime(0) NULL DEFAULT NULL COMMENT '入库时间',
  `past_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `recive_master_id` int(11) NULL DEFAULT NULL COMMENT '接收人ID',
  `sup_price` decimal(24, 6) NULL DEFAULT NULL COMMENT '供应价格',
  `sup_company` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `durg_woker`(`recive_master_id`) USING BTREE,
  CONSTRAINT `durg_woker` FOREIGN KEY (`recive_master_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '药品信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of drug
-- ----------------------------

-- ----------------------------
-- Table structure for drug_order
-- ----------------------------
DROP TABLE IF EXISTS `drug_order`;
CREATE TABLE `drug_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `see_doctor_order_id` int(11) NULL DEFAULT NULL COMMENT '门诊订单ID',
  `drug_id` int(11) NULL DEFAULT NULL COMMENT '药品ID',
  `usage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用法',
  `dose` decimal(24, 6) NULL DEFAULT NULL COMMENT '剂量',
  `price` decimal(24, 6) NULL DEFAULT NULL COMMENT '金额',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `drug_order_drug`(`drug_id`) USING BTREE,
  INDEX `drug_order_see`(`see_doctor_order_id`) USING BTREE,
  CONSTRAINT `drug_order_drug` FOREIGN KEY (`drug_id`) REFERENCES `drug` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `drug_order_see` FOREIGN KEY (`see_doctor_order_id`) REFERENCES `see_doctor_order` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '药品订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of drug_order
-- ----------------------------


-- ----------------------------
-- Table structure for machine
-- ----------------------------
DROP TABLE IF EXISTS `machine`;
CREATE TABLE `machine`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `type` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  `sup_company` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '供应商',
  `sup_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '供应价格',
  `recive_time` datetime(0) NULL DEFAULT NULL COMMENT '接收时间',
  `department_room_id` int(11) NULL DEFAULT NULL COMMENT '部门科室ID',
  `master_id` int(11) NULL DEFAULT NULL COMMENT '主管ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `machine_woker`(`master_id`) USING BTREE,
  INDEX `department_room_id`(`department_room_id`) USING BTREE,
  CONSTRAINT `machine_woker` FOREIGN KEY (`master_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `machine_ibfk_1` FOREIGN KEY (`department_room_id`) REFERENCES `department_room` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '器械信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of machine
-- ----------------------------


-- ----------------------------
-- Table structure for operation_order
-- ----------------------------
DROP TABLE IF EXISTS `operation_order`;
CREATE TABLE `operation_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手术名称',
  `price` decimal(24, 6) NULL DEFAULT NULL COMMENT '价格',
  `order_time` datetime(0) NULL DEFAULT NULL COMMENT '预定时间',
  `sick_person_id` int(11) NULL DEFAULT NULL COMMENT '患者ID',
  `master_id` int(11) NULL DEFAULT NULL COMMENT '医生ID',
  `department_room_id` int(11) NULL DEFAULT NULL COMMENT '手术室ID',
  `see_doctor_order_id` int(11) NULL DEFAULT NULL COMMENT '诊断订单ID',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oper_see`(`see_doctor_order_id`) USING BTREE,
  INDEX `oper_room`(`department_room_id`) USING BTREE,
  INDEX `oper_woker`(`master_id`) USING BTREE,
  INDEX `oper_sick_person`(`sick_person_id`) USING BTREE,
  CONSTRAINT `oper_room` FOREIGN KEY (`department_room_id`) REFERENCES `department_room` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `oper_see` FOREIGN KEY (`see_doctor_order_id`) REFERENCES `see_doctor_order` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `oper_sick_person` FOREIGN KEY (`sick_person_id`) REFERENCES `sick_person` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `oper_woker` FOREIGN KEY (`master_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '手术订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of operation_order
-- ----------------------------


-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `department_room_id` int(11) NULL DEFAULT NULL COMMENT '诊室ID',
  `sick_person_id` int(11) NULL DEFAULT NULL COMMENT '就诊人ID',
  `order_date` date NULL DEFAULT NULL COMMENT '预约日期',
  `order_time` time(0) NULL DEFAULT NULL COMMENT '预约时间',
  `price` decimal(24, 6) NULL DEFAULT NULL COMMENT '挂号费用',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `order_dept_room`(`department_room_id`) USING BTREE,
  INDEX `order_person`(`sick_person_id`) USING BTREE,
  CONSTRAINT `order_dept_room` FOREIGN KEY (`department_room_id`) REFERENCES `department_room` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `order_person` FOREIGN KEY (`sick_person_id`) REFERENCES `sick_person` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '预约信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order
-- ----------------------------


-- ----------------------------
-- Table structure for see_doctor_order
-- ----------------------------
DROP TABLE IF EXISTS `see_doctor_order`;
CREATE TABLE `see_doctor_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `result` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '诊断结果',
  `doctor_id` int(11) NULL DEFAULT NULL COMMENT '医生ID',
  `sick_person_id` int(11) NULL DEFAULT NULL COMMENT '患者ID',
  `department_room_id` int(11) NULL DEFAULT NULL COMMENT '科室ID',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `see_woker`(`doctor_id`) USING BTREE,
  INDEX `see_person`(`sick_person_id`) USING BTREE,
  INDEX `see_dept_room`(`department_room_id`) USING BTREE,
  CONSTRAINT `see_dept_room` FOREIGN KEY (`department_room_id`) REFERENCES `department_room` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `see_person` FOREIGN KEY (`sick_person_id`) REFERENCES `sick_person` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `see_woker` FOREIGN KEY (`doctor_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '就诊信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of see_doctor_order
-- ----------------------------

-- ----------------------------
-- Table structure for sick_bed
-- ----------------------------
DROP TABLE IF EXISTS `sick_bed`;
CREATE TABLE `sick_bed`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `bed_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '病床号',
  `master_id` int(11) NULL DEFAULT NULL COMMENT '管理员ID',
  `department_room_id` int(11) NULL DEFAULT NULL COMMENT '所属病房ID',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sick_bed_room`(`department_room_id`) USING BTREE,
  INDEX `sick_bed_woker`(`master_id`) USING BTREE,
  CONSTRAINT `sick_bed_room` FOREIGN KEY (`department_room_id`) REFERENCES `department_room` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `sick_bed_woker` FOREIGN KEY (`master_id`) REFERENCES `woker` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '病床信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sick_bed
-- ----------------------------

-- ----------------------------
-- Table structure for sick_bed_order
-- ----------------------------
DROP TABLE IF EXISTS `sick_bed_order`;
CREATE TABLE `sick_bed_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '费用',
  `see_doctor_order_id` int(11) NULL DEFAULT NULL COMMENT '门诊订单ID',
  `sick_bed_id` int(11) NULL DEFAULT NULL COMMENT '病床ID',
  `recive_time` datetime(0) NULL DEFAULT NULL COMMENT '接收病人时间',
  `sent_time` datetime(0) NULL DEFAULT NULL COMMENT '病人出院时间',
  `state` int(11) NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `bed_order_bed`(`sick_bed_id`) USING BTREE,
  INDEX `bed_order_see`(`see_doctor_order_id`) USING BTREE,
  CONSTRAINT `bed_order_bed` FOREIGN KEY (`sick_bed_id`) REFERENCES `sick_bed` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bed_order_see` FOREIGN KEY (`see_doctor_order_id`) REFERENCES `see_doctor_order` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '病床信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sick_bed_order
-- ----------------------------


-- ----------------------------
-- Table structure for sick_person
-- ----------------------------
DROP TABLE IF EXISTS `sick_person`;
CREATE TABLE `sick_person`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `card_id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '就诊卡号',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `birthday` datetime(0) NULL DEFAULT NULL COMMENT '生日',
  `gender` int(11) NULL DEFAULT NULL COMMENT '性别',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '患者信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sick_person
-- ----------------------------

-- ----------------------------
-- Table structure for woker
-- ----------------------------
DROP TABLE IF EXISTS `woker`;
CREATE TABLE `woker`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `gender` int(11) NULL DEFAULT NULL COMMENT '性别',
  `department_id` int(11) NULL DEFAULT NULL COMMENT '部门ID',
  `state` int(11) NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `woker_dept`(`department_id`) USING BTREE,
  CONSTRAINT `woker_dept` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工作人员信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of woker
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;