/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/7 17:50:06                            */
/*==============================================================*/


drop table if exists LS_Price;

drop table if exists Worker_info;

drop table if exists car;

drop table if exists complain;

drop table if exists "order";

drop table if exists site;

drop table if exists warehouse;

/*==============================================================*/
/* Table: LS_Price                                              */
/*==============================================================*/
create table LS_Price
(
   Dictance             varchar(30) not null,
   Weight               varchar(10) not null,
   Price                (10,2) not null
);

/*==============================================================*/
/* Table: Worker_info                                           */
/*==============================================================*/
create table Worker_info
(
   id                   int not null auto_increment,
   name                 varchar(10) not null,
   post                 enum('站长','快递员','货车司机','站点工作') not null,
   IDcard               char(18) not null,
   phone_number         char(11) not null,
   distribution         int not null,
   health               varchar(10) not null,
   primary key (id)
);

/*==============================================================*/
/* Table: car                                                   */
/*==============================================================*/
create table car
(
   id                   int not null auto_increment,
   car_number           varchar(8) not null,
   driver               varchar(10) not null,
   type                  enum('三轮车'，'大货车','小型火车'),
   state                enum('运输中'，'到站点','待定'),
   "vehicle condition"  varchar(10),
   belong_siteID        int,
   primary key (id)
);

/*==============================================================*/
/* Table: complain                                              */
/*==============================================================*/
create table complain
(
   id                   int not null auto_increment,
   complain_name        varchar(10),
   complain_because     enum('送货速度太慢','快递员态度不好','发错货','物品有破损','发货慢') not null,
   complain_info        text not null,
   primary key (id)
);

/*==============================================================*/
/* Table: "order"                                               */
/*==============================================================*/
create table "order"
(
   id                   int not null auto_increment,
   order_id             varchar(30) not null,
   state                enum('已发货','未发货','已送达','运输中','遗失','缺件') not null,
   origin               varchar(50) not null,
   destination          carchar(50) not null,
   sender               varchar(30) not null,
   addressee            varchar(10) not null,
   sender_phone         char(11) not null,
   addressee_phone      char(11) not null,
   logistics_price      decimal(10,2) not null,
   courier_id           int not null,
   courier_phone        char(11) not null,
   open_time            datetime not null default 'now()',
   plan_arrive_time     datetime not null,
   arrive_time          datetime not null,
   customer_estimate    text,
   primary key (id)
);

/*==============================================================*/
/* Table: site                                                  */
/*==============================================================*/
create table site
(
   id                   int not null auto_increment,
   Administrator_id     int,
   site_name            varchar(30) not null,
   site_address         varchar(50) not null,
   order_number         iint not null,
   lack_number          int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: warehouse                                             */
/*==============================================================*/
create table warehouse
(
   id                   int not null auto_increment,
   address              varchar(30) not null,
   Stock                int not null,
   belong_SiteID        int not null,
   primary key (id)
);

alter table car add constraint FK_Reference_4 foreign key (belong_siteID)
      references site (id) on delete restrict on update restrict;

alter table complain add constraint FK_Reference_1 foreign key (complain_name)
      references "order" (order_id) on delete restrict on update restrict;

alter table "order" add constraint FK_Reference_2 foreign key (courier_id, courier_phone)
      references Worker_info (id, phone_number) on delete restrict on update restrict;

alter table warehouse add constraint FK_Reference_3 foreign key (belong_SiteID)
      references site (id) on delete restrict on update restrict;

