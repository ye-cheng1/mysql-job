/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     2021/9/7 17:39:37                            */
/*==============================================================*/




drop table if exists clientinfo;

drop table if exists extra;

drop table if exists market;

drop table if exists orderinfo;

drop table if exists product;

drop table if exists product_order;

drop table if exists rawinfo;

drop table if exists staffinfo;

drop table if exists workshopinfo;

drop table if exists plan;

/*==============================================================*/
/* Table: clientinfo                                            */
/*==============================================================*/
create table clientinfo 
(
   clientid             int                            not null,
   clientname           varchar(10)                    null,
   company              varchar(30)                    null,
   time                     datetime                       null,
   constraint PK_CLIENTINFO primary key clustered (clientid)
);

/*==============================================================*/
/* Table: extra                                                 */
/*==============================================================*/
create table extra 
(
   money1               decimal                        null,
   money2               decimal                        null,
   money3               decimal                        null,
   money4               decimal                        null
);

/*==============================================================*/
/* Table: market                                                */
/*==============================================================*/
create table market 
(
   id                   int                            not null,
   mane                 varchar(10)                    null,
   money                decimal                        null,
   number               int                            null,
   channel              varchar(20)                    null,
   constraint PK_MARKET primary key clustered (id)
);

/*==============================================================*/
/* Table: orderinfo                                             */
/*==============================================================*/
create table orderinfo 
(
   orderid              int                            not null,
   clientid             int                            null,
   time               datetime                       null,
   constraint PK_ORDERINFO primary key clustered (orderid)
);

/*==============================================================*/
/* Table: product                                               */
/*==============================================================*/
create table product 
(
   productid            int                            not null,
   workshopid           int                            null,
   productname          varchar(10)                    null,
   technology           varchar(30)                    null,
   constraint PK_PRODUCT primary key clustered (productid)
);

/*==============================================================*/
/* Table: product_order                                         */
/*==============================================================*/
create table product_order 
(
   productid            int                            null,
   orderid              int                            null
);

/*==============================================================*/
/* Table: rawinfo                                               */
/*==============================================================*/
create table rawinfo 
(
   rawid                int                            not null,
   name                 varchar(10)                    null,
   money                decimal                        null,
   number               int                            null,
   productid            int                            null,
   time               datetime                       null,
   constraint PK_RAWINFO primary key clustered (rawid)
);

/*==============================================================*/
/* Table: staffinfo                                             */
/*==============================================================*/
create table staffinfo 
(
   staff_id             int                            not null,
   workshopid           int                            null,
   post                 varchar(20)                    null,
   staff_name           varchar(10)                    null,
   staff_sex            varchar(1)                     null,
   staff_age            int                            null,
   time               datetime                       null,
   constraint PK_STAFFINFO primary key clustered (staff_id)
);

/*==============================================================*/
/* Table: workshopinfo                                          */
/*==============================================================*/
create table workshopinfo 
(
   workshopid           int                            not null,
   workshopname         varchar(20)                    null,
   chargeid             int                            null,
   workshopnumber       int                            null,
   constraint PK_WORKSHOPINFO primary key clustered (workshopid)
);

/*==============================================================*/
/* Table: plan                                                  */
/*==============================================================*/
create table plan
(
   id                   int                            not null,
   time               datetime                       null,
   number1              int                            null,
   number2              int                            null,
   workshop1            int                            null,
   constraint PK_plan primary key clustered (id)
);

alter table orderinfo
   add constraint FK_ORDERINF_REFERENCE_CLIENTIN foreign key (clientid)
      references clientinfo (clientid)
      on update restrict
      on delete restrict;

alter table product
   add constraint FK_PRODUCT_REFERENCE_WORKSHOP foreign key (workshopid)
      references workshopinfo (workshopid)
      on update restrict
      on delete restrict;

alter table product_order
   add constraint FK_PRODUCT__REFERENCE_PRODUCT foreign key (productid)
      references product (productid)
      on update restrict
      on delete restrict;

alter table product_order
   add constraint FK_PRODUCT__REFERENCE_ORDERINF foreign key (orderid)
      references orderinfo (orderid)
      on update restrict
      on delete restrict;

alter table rawinfo
   add constraint FK_RAWINFO_REFERENCE_PRODUCT foreign key (productid)
      references product (productid)
      on update restrict
      on delete restrict;

alter table staffinfo
   add constraint FK_STAFFINF_REFERENCE_WORKSHOP foreign key (workshopid)
      references workshopinfo (workshopid)
      on update restrict
      on delete restrict;

alter table plan
   add constraint FK_plan_REFERENCE_WORKSHOP foreign key (workshop1)
      references workshopinfo (workshopid)
      on update restrict
      on delete restrict;

if exists(select 1 from sys.sysforeignkey where role='FK_ORDERINF_REFERENCE_CLIENTIN') then
    alter table orderinfo
       delete foreign key FK_ORDERINF_REFERENCE_CLIENTIN
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_PRODUCT_REFERENCE_WORKSHOP') then
    alter table product
       delete foreign key FK_PRODUCT_REFERENCE_WORKSHOP
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_PRODUCT__REFERENCE_PRODUCT') then
    alter table product_order
       delete foreign key FK_PRODUCT__REFERENCE_PRODUCT
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_PRODUCT__REFERENCE_ORDERINF') then
    alter table product_order
       delete foreign key FK_PRODUCT__REFERENCE_ORDERINF
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_RAWINFO_REFERENCE_PRODUCT') then
    alter table rawinfo
       delete foreign key FK_RAWINFO_REFERENCE_PRODUCT
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_STAFFINF_REFERENCE_WORKSHOP') then
    alter table staffinfo
       delete foreign key FK_STAFFINF_REFERENCE_WORKSHOP
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_plan_REFERENCE_WORKSHOP') then
    alter table plan
       delete foreign key FK_plan_REFERENCE_WORKSHOP
end if;