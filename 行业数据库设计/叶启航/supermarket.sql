/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 9:47:24                             */
/*==============================================================*/


drop table if exists Inventory_information_sheet;

drop table if exists Purchase_information_sheet;

drop table if exists Sales_information_sheet;

drop table if exists Supplier_information_Sheet;

drop table if exists Table_of_working_areas;

drop table if exists employee_information_table;

drop table if exists goods_info;

drop table if exists member_info;

/*==============================================================*/
/* Table: Inventory_information_sheet                           */
/*==============================================================*/
create table Inventory_information_sheet
(
   stock_number         int not null,
   goods_id             int not null,
   number               int not null,
   warehouse_entry_time datetime not null,
   expiration_time      datetime not null,
   remark               text,
   primary key (stock_number)
);

/*==============================================================*/
/* Table: Purchase_information_sheet                            */
/*==============================================================*/
create table Purchase_information_sheet
(
   order_id             int not null,
   goods_id             int,
   purchasing_cost      decimal not null,
   number               int not null,
   total_money          decimal not null,
   data                 datetime not null,
   remark               text,
   primary key (order_id)
);

/*==============================================================*/
/* Table: Sales_information_sheet                               */
/*==============================================================*/
create table Sales_information_sheet
(
   Sales_order          int not null,
   goods_id             int not null,
   number               int not null,
   price                decimal not null,
   data                 datetime not null,
   remark               text,
   primary key (Sales_order)
);

/*==============================================================*/
/* Table: Supplier_information_Sheet                            */
/*==============================================================*/
create table Supplier_information_Sheet
(
   Number_of_supply     int not null,
   name                 varchar(20) not null,
   Contact              varchar(10) not null,
   telephone            int not null,
   address              text not null,
   remark               text,
   primary key (Number_of_supply)
);

/*==============================================================*/
/* Table: Table_of_working_areas                                */
/*==============================================================*/
create table Table_of_working_areas
(
   Work_area_Number     int not null,
   Work_area_Name       varchar(20) not null,
   remark               text,
   primary key (Work_area_Number)
);

/*==============================================================*/
/* Table: employee_information_table                            */
/*==============================================================*/
create table employee_information_table
(
   id                   int not null auto_increment,
   name                 varchar(10) not null,
   Work_area_Number     int not null,
   remark               text,
   primary key (id)
);

/*==============================================================*/
/* Table: goods_info                                            */
/*==============================================================*/
create table goods_info
(
   goods_id             int not null,
   Number_of_supply     int not null,
   goods_name           varchar(20) not null,
   goods_price          decimal not null,
   price                decimal not null,
   remark               text,
   primary key (goods_id)
);

/*==============================================================*/
/* Table: member_info                                           */
/*==============================================================*/
create table member_info
(
   member_id            int not null,
   member_card          int not null,
   member_name          varchar(10) not null,
   member_telephone     int not null,
   number               int not null,
   primary key (member_id)
);

alter table Inventory_information_sheet add constraint FK_Reference_5 foreign key (goods_id)
      references goods_info (goods_id) on delete restrict on update restrict;

alter table Purchase_information_sheet add constraint FK_Reference_3 foreign key (goods_id)
      references goods_info (goods_id) on delete restrict on update restrict;

alter table Sales_information_sheet add constraint FK_Reference_4 foreign key (goods_id)
      references goods_info (goods_id) on delete restrict on update restrict;

alter table employee_information_table add constraint FK_Reference_1 foreign key (Work_area_Number)
      references Table_of_working_areas (Work_area_Number) on delete restrict on update restrict;

alter table goods_info add constraint FK_Reference_2 foreign key (Number_of_supply)
      references Supplier_information_Sheet (Number_of_supply) on delete restrict on update restrict;

