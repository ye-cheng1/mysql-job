CREATE TABLE `userInfo` (
`customerID` int(11) NOT NULL,
`customerName` varchar(255) NULL,
`PID` varchar(255) NULL,
`telephone` varchar(255) NULL,
`address` text NULL,
PRIMARY KEY (`customerID`) 
);

CREATE TABLE `CardInfo` (
`cardID` int(11) NOT NULL,
`savingID` int(11) NULL,
`openDate` datetime NULL ON UPDATE CURRENT_TIMESTAMP,
`openMoney` decimal NULL,
`balance` decimal NULL,
`password` varchar(255) NULL,
`IsReportLoss` varchar(255) NULL,
`customerID` int(11) NULL,
`curID` int(11) NULL,
PRIMARY KEY (`cardID`) 
);

CREATE TABLE `table_1` (
);

CREATE TABLE `tradeInfo` (
`transDate` datetime NULL ON UPDATE CURRENT_TIMESTAMP,
`cardID` int(11) NULL,
`transType` varchar(255) NULL,
`transMoney` decimal NULL,
`remark` text NULL
);

CREATE TABLE `deposit` (
`savingID` int(11) NOT NULL,
`savingName` varchar(255) NULL,
`curID` int(11) NULL,
PRIMARY KEY (`savingID`) 
);


ALTER TABLE `CardInfo` ADD CONSTRAINT `fk_CardInfo_userInfo_1` FOREIGN KEY (`customerID`) REFERENCES `userInfo` (`customerID`);
ALTER TABLE `CardInfo` ADD CONSTRAINT `fk_CardInfo_tradeInfo_1` FOREIGN KEY (`cardID`) REFERENCES `tradeInfo` (`cardID`);
ALTER TABLE `CardInfo` ADD CONSTRAINT `fk_CardInfo_deposit_1` FOREIGN KEY (`curID`) REFERENCES `deposit` (`curID`);

