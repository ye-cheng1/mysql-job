/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/7 17:50:09                            */
/*==============================================================*/


drop table if exists Enterprise;

drop table if exists Major;

drop table if exists class_choose;

drop table if exists classroom;

drop table if exists club;

drop table if exists conplaint;

drop table if exists department;

drop table if exists dormitory;

drop table if exists elective;

drop index Index_1 on studentinfo;

drop table if exists studentinfo;

drop table if exists tercher;

/*==============================================================*/
/* Table: Enterprise                                            */
/*==============================================================*/
create table Enterprise
(
   id                   int not null auto_increment,
   name                 varchar(20),
   detail               text,
   place                varchar(10),
   primary key (id)
);

/*==============================================================*/
/* Table: Major                                                 */
/*==============================================================*/
create table Major
(
   id                   int not null,
   name                 varchar(10),
   d_id                 int,
   enrollment           int,
   access_line          int,
   fare                 int,
   primary key (id)
);

/*==============================================================*/
/* Table: class_choose                                          */
/*==============================================================*/
create table class_choose
(
   id                   int,
   student_id           int,
   classid              int
);

/*==============================================================*/
/* Table: classroom                                             */
/*==============================================================*/
create table classroom
(
   id                   int not null auto_increment,
   major_class          varchar(10),
   configeration        varchar(60),
   borrow               enum(0,1),
   primary key (id)
);

/*==============================================================*/
/* Table: club                                                  */
/*==============================================================*/
create table club
(
   id                   int not null,
   number               varchar(10),
   name                 varchar(10),
   detail               text,
   scale                int,
   primary key (id)
);

/*==============================================================*/
/* Table: conplaint                                             */
/*==============================================================*/
create table conplaint
(
   id                   int not null,
   place                varchar(60),
   detail               varchar(60),
   solve                enum(0,1),
   primary key (id)
);

/*==============================================================*/
/* Table: department                                            */
/*==============================================================*/
create table department
(
   id                   int not null,
   name                 varchar(10),
   master               varchar(10),
   detail               text,
   primary key (id)
);

/*==============================================================*/
/* Table: dormitory                                             */
/*==============================================================*/
create table dormitory
(
   id                   int not null,
   build_num            int,
   dormitory            int,
   configeration        varchar(60),
   department_id        int,
   max_num              int,
   primary key (id)
);

/*==============================================================*/
/* Table: elective                                              */
/*==============================================================*/
create table elective
(
   id                   int not null auto_increment,
   name                 varchar(10),
   tercher_id           int,
   primary key (id)
);

/*==============================================================*/
/* Table: studentinfo                                           */
/*==============================================================*/
create table studentinfo
(
   id                   int not null auto_increment,
   name                 varchar(10),
   M_id                 int,
   age                  tinyint,
   hight                tinyint,
   wight                tinyint,
   gender               enum(0,1),
   birthday             datetime,
   IDcode               varchar(18),
   province             varchar(10),
   country              varchar(10),
   city                 varchar(10),
   place_detail         varchar(20),
   bankcode             varchar(21),
   poor                 enum(0,1),
   graduate             enum(0,1),
   get_job              enum(0,1),
   student_num          varchar(10),
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on studentinfo
(
   student_num
);

/*==============================================================*/
/* Table: tercher                                               */
/*==============================================================*/
create table tercher
(
   id                   int not null auto_increment,
   name                 varchar(10),
   number               varchar(10),
   age                  tinyint,
   major_id             int,
   year                 int,
   evaluate             enum(0,1),
   primary key (id)
);

alter table Major add constraint FK_Reference_1 foreign key (d_id)
      references department (id) on delete restrict on update restrict;

alter table class_choose add constraint FK_Reference_3 foreign key (id)
      references elective (id) on delete restrict on update restrict;

alter table class_choose add constraint FK_Reference_4 foreign key (student_id)
      references studentinfo (id) on delete restrict on update restrict;

alter table studentinfo add constraint FK_Reference_2 foreign key (M_id)
      references Major (id) on delete restrict on update restrict;

