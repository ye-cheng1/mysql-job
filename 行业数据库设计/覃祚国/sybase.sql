/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/7 17:11:33                            */
/*==============================================================*/



/*==============================================================*/
/* Table: vip_level                                             */
/*==============================================================*/

create database e_commerce_sys;

use e_commerce_sys;

create table vip_level
(
   vip_level_id         int not null,
   vip_level_name       varchar(10) not null default '无',
   vip_pont             int not null,
   effective_date       int not null,
   primary key (vip_level_id)
);

drop table if exists user_login;

/*==============================================================*/
/* Table: user_login                                            */
/*==============================================================*/
create table user_login
(
   user_login_id        int not null,
   vip_level_id         int,
   user_login_name      varchar(20) not null,
   user_login_password  varchar(20) not null,
   user_last_login      datetime not null,
   user_status          tinyint not null default 0,
   primary key (user_login_id),
   FOREIGN key (vip_level_id) REFERENCES vip_level(vip_level_id)
);

/*==============================================================*/
/* Table: user_info                                             */
/*==============================================================*/
create table user_info
(
   user_info_id         int not null auto_increment,
   user_login_id        int not null,
   user_identity_card_no varchar(20) not null,
   user_real_name       varchar(20) not null,
   user_phone           int not null,
   user_gender          enum('男','女') default '男',
   user_register_time   datetime not null,
   primary key (user_info_id),
   FOREIGN key (user_login_id) REFERENCES user_login(user_login_id)
);


/*==============================================================*/
/* Table: user_address                                          */
/*==============================================================*/
create table user_address
(
   user_address_id      int not null auto_increment,
   user_info_id         int not null,
   province             varchar(10) not null,
   city                 varchar(10) not null,
   district             varchar(10) not null,
   street               varchar(10) not null,
   specific_address     varchar(50),
   primary key (user_address_id),
   FOREIGN KEY (user_info_id) REFERENCES user_info(user_info_id)
);

/*==============================================================*/
/* Table: bank_card                                             */
/*==============================================================*/
create table bank_card
(
   bank_card_id         int not null auto_increment,
   user_login_id        int not null,
   bank_name            enum('农行','工行','建行') not null,
   card_no              int not null,
   primary key (bank_card_id),
   FOREIGN key (user_login_id) REFERENCES user_login(user_login_id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on bank_card
(
   card_no
);

/*==============================================================*/
/* Table: store_info                                           */
/*==============================================================*/
create table store_info
(
   store_id             int not null auto_increment,
   user_login_id        int not null,
   store_name           varchar(20) not null,
   register_time        datetime not null,
   good_review_rate     tinyint,
   province             varchar(10) not null,
   city                 varchar(10) not null,
   district             varchar(10) not null,
   street               varchar(10) not null,
   specific_address     varchar(50),
   store_favorited      int,
   primary key (store_id),
   FOREIGN key (user_login_id) REFERENCES user_login(user_login_id)
);

/*==============================================================*/
/* Table: "'店铺会员关系表"                                            */
/*==============================================================*/
create table store_user_vip
(
   store_user_vip_id    int not null auto_increment,
   store_id             int not null,
   user_login_id        int not null,
   primary key (store_user_vip_id),
   FOREIGN key (store_id) REFERENCES store_info(store_id),
   FOREIGN key (user_login_id) REFERENCES user_login(user_login_id)
);


/*==============================================================*/
/* Table: good_category                                         */
/*==============================================================*/
create table good_category
(
   good_category_id     int not null,
   good_category_name   varbinary(20) not null,
   primary key (good_category_id)
);

/*==============================================================*/
/* Table: product_info                                          */
/*==============================================================*/
create table product_info
(
   product_id           int not null auto_increment,
   good_category_id     int not null,
   store_id             int not null,
   product_name         varchar(30) not null,
   product_intro        MEDIUMTEXT,
   price                decimal(10,2) not null,
   discount             tinyint,
   cost                 decimal(10,2),
   stock                int not null,
   on_sale_time         datetime,
   product_favorited    int,
   product_good_review  tinyint,
   primary key (product_id),
   FOREIGN key (good_category_id) REFERENCES good_category(good_category_id),
   FOREIGN key (store_id) REFERENCES store_info(store_id)
);

/*==============================================================*/
/* Table: shopping_cart                                         */
/*==============================================================*/
create table shopping_cart
(
   cart_id              int not null auto_increment,
   user_login_id        int not null,
   updata_time          datetime,
   primary key (cart_id),
   FOREIGN key (user_login_id) REFERENCES user_login(user_login_id)
);

/*==============================================================*/
/* Table: cart_product                                          */
/*==============================================================*/
create table cart_product
(
   card_product_id      int not null auto_increment,
   cart_id              int not null,
   product_id           int not null,
   primary key (card_product_id),
   FOREIGN key (cart_id) REFERENCES shopping_cart(cart_id),
   FOREIGN key (product_id) REFERENCES product_info(product_id)
);


/*==============================================================*/
/* Table: good_review_info                                      */
/*==============================================================*/
create table good_review_info
(
   good_review_id       int not null,
   product_id           int not null,
   user_login_id        int not null,
   review_level         enum('好评','中评','差评') default '好评',
   review_text           MEDIUMTEXT,
   time                 datetime not null,
   primary key (good_review_id),
   FOREIGN key (product_id) REFERENCES product_info(product_id),
   FOREIGN key (user_login_id) REFERENCES user_login(user_login_id)
);

/*==============================================================*/
/* Table: logistics_info                              */
/*==============================================================*/
create table logistics_info(
	logistics_info_id int primary key auto_increment,
	send_company enum('顺丰','圆通','中通','韵达'),
	send_time datetime,
	estimate_time datetime,
	get_time datetime,
	province varchar(10),
	city varchar(10)
);



/*==============================================================*/
/* Table: order_info                                            */
/*==============================================================*/
create table order_info
(
   order_id             int not null auto_increment,
   user_login_id        int not null,
   order_status         enum('已取消','未付款','已付款','已发货','交易成功','交易关闭') not null,
   good_num             int not null,
   logistics_price      decimal(10,2) not null,
   order_price          decimal(10,2) not null,
   order_discount       decimal(10,2) not null,
   pay_type             enum('支付宝','微信','绑定银行卡') not null,
   pay_time             datetime not null,
   create_time          datetime not null,
   last_time            datetime not null,
   weight               datetime not null,
   province             varchar(10) not null,
   city                 varchar(10) not null,
   district             varchar(10) not null,
   street               varchar(10) not null,
   specific_address     varchar(50) not null,
   logistics_info_id    int not null,
   primary key (order_id),
   FOREIGN key (user_login_id) REFERENCES user_login(user_login_id),
   FOREIGN key (logistics_info_id) REFERENCES logistics_info(logistics_info_id)
);

/*==============================================================*/
/* Table: order_product                                         */
/*==============================================================*/
create table order_product
(
   order_product_id     int not null auto_increment,
   order_id             int not null,
   product_id           int not null,
   price                decimal(10,2)  not null,
   primary key (order_product_id),
   FOREIGN key (order_id) REFERENCES order_info(order_id),
   FOREIGN key (product_id) REFERENCES product_info(product_id)
);


/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on user_info
(
   user_login_id
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create unique index Index_2 on user_info
(
   user_identity_card_no
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on bank_card
(
   card_no
);



/*==============================================================*/
/* Index: Index_4                                               */
/*==============================================================*/
create unique index Index_4 on good_category
(
   good_category_name
);


