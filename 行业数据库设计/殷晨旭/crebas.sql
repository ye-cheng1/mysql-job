/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/7 16:42:25                            */
/*==============================================================*/


drop table if exists goods;

drop table if exists merchant;

drop table if exists "order-form";

drop table if exists rider;

drop index Index_1 on user;

drop table if exists user;

/*==============================================================*/
/* Table: goods                                                 */
/*==============================================================*/
create table goods
(
   ID                   int not null,
   goods_ID             int not null,
   name                 varchar(50) not null,
   primary key (ID)
);

/*==============================================================*/
/* Table: merchant                                              */
/*==============================================================*/
create table merchant
(
   ID                   int not null auto_increment,
   name                 varchar(50) not null,
   address              varchar(200) not null,
   type                 varchar(10),
   primary key (ID)
);

/*==============================================================*/
/* Table: "order-form"                                          */
/*==============================================================*/
create table "order-form"
(
   ID                   int not null,
   merchant_ID          int not null,
   user_ID              int not null,
   rider_ID             int not null,
   goods_ID             int not null,
   evaluate             varchar(50),
   "condition"          varchar(10) default '����',
   datetime             datetime,
   primary key (ID)
);

/*==============================================================*/
/* Table: rider                                                 */
/*==============================================================*/
create table rider
(
   ID                   int not null auto_increment,
   name                 varchar(50) not null,
   credit               varchar(10) not null,
   health               varchar(10) not null,
   phone                char(11) not null,
   primary key (ID)
);

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
create table user
(
   ID                   int not null auto_increment,
   address              varchar(200) not null,
   name                 varchar(50) not null,
   phone                char(11) not null,
   primary key (ID)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on user
(
   phone
);

alter table goods add constraint FK_Reference_1 foreign key (goods_ID)
      references merchant (ID) on delete restrict on update restrict;

alter table "order-form" add constraint FK_Reference_2 foreign key (merchant_ID)
      references merchant (ID) on delete restrict on update restrict;

alter table "order-form" add constraint FK_Reference_3 foreign key (user_ID)
      references user (ID) on delete restrict on update restrict;

alter table "order-form" add constraint FK_Reference_4 foreign key (rider_ID)
      references rider (ID) on delete restrict on update restrict;

alter table "order-form" add constraint FK_Reference_5 foreign key (goods_ID)
      references goods (ID) on delete restrict on update restrict;

