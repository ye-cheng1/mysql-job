/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     2021/9/8 16:45:53                            */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_出诊时间表_REFERENCE_专家表') then
    alter table 出诊时间表
       delete foreign key FK_出诊时间表_REFERENCE_专家表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_科室表_REFERENCE_专家表') then
    alter table 科室表
       delete foreign key FK_科室表_REFERENCE_专家表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_预约记录表_REFERENCE_出诊时间表') then
    alter table 预约记录表
       delete foreign key FK_预约记录表_REFERENCE_出诊时间表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_预约记录表_REFERENCE_患者表') then
    alter table 预约记录表
       delete foreign key FK_预约记录表_REFERENCE_患者表
end if;

drop table if exists 专家表;

drop table if exists 出诊时间表;

drop table if exists 患者表;

drop table if exists 科室表;

drop table if exists 预约记录表;

/*==============================================================*/
/* Table: 专家表                                                   */
/*==============================================================*/
create table 专家表 
(
   id                   int                            not null,
   departmentID         int                            null,
   name                 varchar(10)                    null,
   gender               char(1)                        null,
   imgage               varchar(255)                   null,
   jop                  varchar(255)                   null,
   specialty            text                           null,
   constraint PK_专家表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 出诊时间表                                                 */
/*==============================================================*/
create table 出诊时间表 
(
   id                   int                            not null,
   expertId             int                            null,
   expertTime           datetime                       null,
   constraint PK_出诊时间表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 患者表                                                   */
/*==============================================================*/
create table 患者表 
(
   id                   int                            not null,
   name                 varchar(10)                    null,
   gender               char(10)                       null,
   PID                  varchar(18)                    null,
   telephone            varchar(11)                    null,
   password             varchar(255)                   null,
   ispwd                varchar(255)                   null,
   address              text                           null,
   constraint PK_患者表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 科室表                                                   */
/*==============================================================*/
create table 科室表 
(
   id                   int                            null,
   type                 varchar(255)                   null,
   name                 varchar(10)                    null
);

/*==============================================================*/
/* Table: 预约记录表                                                 */
/*==============================================================*/
create table 预约记录表 
(
   id                   int                            not null,
   userID               int                            null,
   expertTimeID         int                            null,
   createTime           datetime                       null,
   constraint PK_预约记录表 primary key clustered (id)
);

alter table 出诊时间表
   add constraint FK_出诊时间表_REFERENCE_专家表 foreign key (expertId)
      references 专家表 (id)
      on update restrict
      on delete restrict;

alter table 科室表
   add constraint FK_科室表_REFERENCE_专家表 foreign key (id)
      references 专家表 (id)
      on update restrict
      on delete restrict;

alter table 预约记录表
   add constraint FK_预约记录表_REFERENCE_出诊时间表 foreign key (userID)
      references 出诊时间表 (id)
      on update restrict
      on delete restrict;

alter table 预约记录表
   add constraint FK_预约记录表_REFERENCE_患者表 foreign key (expertTimeID)
      references 患者表 (id)
      on update restrict
      on delete restrict;

